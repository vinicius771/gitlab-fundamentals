# GitLab fundamentals

Learning the GitLab Fundamentals **UBUNTU**

- Git (I have already installed it previously)
- Forking
- Creating SSH Key (I have already one, I just add it on GitLab)
  `$ cat ~/.ssh/id_rsa.pub`
- GitLab Runner
- GitLab CI/CD

## GitLab Runner

Used in GitLab CI (Continuous Integration Services) to run jobs and send results back to GitLab.

1. Installing GitLab Runner

- Added the official GitLab repository:

```bash
# For Debian/Ubuntu/Mint
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

- Installed the latest version of GitLab Runner

```bash
# For Debian/Ubuntu/Mint
sudo apt-get install gitlab-runner
```

Checking the success of the instalation:

```bash
gitlab-runner --version
```

Starting / **stopping (CTRL + C)** Gitlab runner

```bash
sudo gitlab-runner run
```

2. Registering GitLab Runner (Linux)

```bash
sudo gitlab-runner register
```

`Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):`

```bash
https://gitlab.com/
```

`Please enter the gitlab-ci token for this runner:`
This token will be available in your GitLab dashboard on Setttings > CI / CD > Runners > Expand and search for **specific runners**.

`Please enter the gitlab-ci description for this runner:`

```bash
example-runner
```

`Please enter the gitlab-ci tags for this runner (comma separated):`

```bash
ci, bash
```

```bash
Whether to run untagged builds [true/false]:
[false]:
Whether to lock the Runner to current project [true/false]:
[true]:
```

`Please enter the executor: docker-ssh+machine, kubernetes, docker, shell, ssh, virtualbox, docker+machine, docker-ssh, parallels:`

```bash
shell
```

`Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!`

Checking if runner is activated in the project:

```
sudo gitlab-runner start
```

Go to Setttings > CI / CD > Runners > Expand

## GitLab CI / CD

1. What is GitLab CI/CD
   Dev > Application Test > Integration Test > Acceptance Test

Open source CI service included with GitLab

1. Creating `.gitlab-ci.yml` file.

Create this file in the root folder of the project / repository.
Remember the tag must match with one of the project tags

Example Content:

```yml
demo_job:
  tags:
    - bash
   script:
    - echo Job Working
```

Check for the yml validation at: [Yaml Lint](http://www.yamllint.com/)

Commit and push the changes to the GitLab remote repository.

```
git add .gitlab-ci.yml
git commit -m "CI/CD config file"
git push -u origin main
```

Make sure the runner is already created (Previous step).

1. Starting the `runner`.

```bash
sudo gitlab-runner start
```

Verifying the runner state:

`gitlab-runner verify`

```
Verifying runner... is alive                        runner=fee9938e
```

Making changes to the project in order to test the runner:

In my case, I am going to add a new file and push the changes to the GitLab remote repository.

```
vim example-file.txt
```

I have had failed when trying to run the pipelines. The reason is described below:

> User validation required.

> To use free pipeline minutes on shared runners, you’ll need to validate your account with a credit or debit card. If you prefer not to provide one, you can run pipelines by bringing your own runners and disabling shared runners for your project. This is required to discourage and reduce abuse on GitLab infrastructure. GitLab will not charge or store your card, it will only be used for validation

So I have validated my credit card number ...

> User successfully validated

> Your user account has been successfully validated. You can now use free pipeline minutes.

... pushed the changes again and everything worked fine on [dashboard]:

CI / CD > Jobs > passed 


```bash
Running with gitlab-runner 10.1.1 (a0152c4c)
  on example-runner (NEVNJWVF)
Using Shell executor...
Running on uchiha... 00:11
Cloning repository... 00:04
Cloning into '/home/gitlab-runner/builds/...'...
Checking out bb0d7957 as main...
Skipping Git submodules setup
$ echo Job Working 00:03
Job Working
```

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/vinicius771/gitlab-fundamentals.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://docs.gitlab.com/ee/user/application_security/sast/)

---

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:9559a46d687082f0e8cd91f53aabb480?https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name

Choose a self-explaining name for your project.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
